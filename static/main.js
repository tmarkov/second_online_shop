'use strict';

/*
 * Gets the CSRF cookie
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        var csrftoken = getCookie('csrftoken');
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function removeItem(itemId) {
    if (sessionStorage.basket) {
        let basket = JSON.parse(sessionStorage.basket);
        let $productList = $('#product_list');
        let listItem;
        let index;

        for (let prod of basket) {
            // If the product has already been added, replace with the new
            // instance as it might contain changes.
            if (prod.pId === itemId) {
                index = basket.indexOf(prod);
                    basket.splice(index, 1);
            }
        }
        sessionStorage.basket = JSON.stringify(basket);

        if (basket.length > 0) {
            $productList.html("");
            for (let prod of basket) {
                listItem = `<tr id="list-${prod.pId}">\n` +
                           `  <td>${prod.prodName}</td>\n` +
                           `  <td>${prod.quantity}</td>\n` +
                           `  <td>${parseInt(prod.quantity)*parseFloat(prod.price)}</td>\n` +
                           `  <td><button class="btn button-primary" onclick="removeItem('${prod.pId}')">Изтрий</button></td>\n` +
                           `</tr>`;
                $productList.append(listItem);
            }
        }
        else {
            sessionStorage.removeItem("basket");
            $('#table').hide();
            $('#order_form').hide();
            $('#dialog_text').show();
        }
    }
}

$('.add_basket_act').click((event) => {
    let product;
    let basket;
    let inserted = false;
    let prodId = $(event.currentTarget).attr('data-product-id');
    let prodName = $(event.currentTarget).siblings('#prodName').text();
    let prodPrice = $(event.currentTarget).siblings('#priceTag').children('#prodPrice').text();
    let prodQnt = $(event.currentTarget).siblings('.prodQnt').val();

    if (Number.isInteger(parseInt(prodId)) && prodName && prodPrice && prodQnt) {
        product = {
            'pId': prodId,
            'prodName': prodName,
            'price': prodPrice,
            'quantity': prodQnt,
        };

        if (sessionStorage.basket) {
            basket = JSON.parse(sessionStorage.basket);
            for (let prod of basket) {
                // If the product has already been added, replace with the new
                // instance as it might contain changes.
                if (prod.pId === product.pId) {
                    basket[basket.indexOf(prod)] = product;
                    inserted = true;
                }
            }
            if (!inserted) {
                basket.push(product);
            }
            sessionStorage.basket = JSON.stringify(basket);
        }
        else {
            sessionStorage.basket = JSON.stringify([product]);
        }
    }
    else {
        if (!prodQnt) {
            window.alert("Въведете количество.")
        }
        else {
            window.alert("Възникна грешка при добавяне на продукта.")
        }
    }
});

$('#basket_button').click(() => {
    if (sessionStorage.basket) {
        let basket = JSON.parse(sessionStorage.basket);
        let $productList = $('#product_list');
        let listItem;

        $productList.html("");

        $('#table').show();
        $('#order_form').show();
        $('#dialog_text').hide();

        for (let prod of basket) {
            listItem = `<tr id="list-${prod.pId}">\n` +
                       `  <td>${prod.prodName}</td>\n` +
                       `  <td>${prod.quantity}</td>\n` +
                       `  <td>${parseInt(prod.quantity)*parseFloat(prod.price)}</td>\n` +
                       `  <td><button class="btn button-primary" onclick="removeItem('${prod.pId}')">Изтрий</button></td>\n` +
                       `</tr>`;
            $productList.append(listItem);
        }
    }
});


$('#clear_basket').click(() => {
    if (sessionStorage.basket) {
        let $productList = $('#product_list');

        sessionStorage.removeItem("basket");
        $productList.html("");
        $('#table').hide();
        $('#order_form').hide();
        $('#dialog_text').show();
    }
});

$('#makeOrder').click(() => {
    if (sessionStorage.basket) {
        let basket = JSON.parse(sessionStorage.basket);
        let $productList = $('#product_list');
        let uRequestData = {products: basket};

        $.ajax({
            type: "POST",
            url: "http://localhost:8000/order/",
            // The key needs to match your method's input parameter (case-sensitive).
            data: JSON.stringify(uRequestData),
            credentials: 'include',
            xhrFields: {
                withCredentials: true,
            }
        }).then((data, xhr) => {
            console.log("something");
            //console.log(data);
            console.log(xhr.getResponseHeader('Location'));
            if (data.redirect) {
                console.log("inside");
                // data.redirect contains the string URL to redirect to
                window.location.href = "/login/";
            }
            else {
                sessionStorage.removeItem("basket");
                $productList.html("");
                $('#table').hide();
                $('#order_form').hide();
                $('#dialog_text').show();
                window.alert("Успешна поръчка.")
            }
        });
    }
    else {
        window.alert("Нямате продукти в количката.")
    }

});






























