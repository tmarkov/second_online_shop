from django.db import models

from accounts import models as account_models
from shop import models as shop_models


class Order(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    account = models.ForeignKey(account_models.Account,
                                on_delete=models.CASCADE)
    products = models.ManyToManyField(shop_models.Product)

