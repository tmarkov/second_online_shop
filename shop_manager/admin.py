from django.contrib import admin

from shop_manager import models


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):

    class Meta:
        order = models.Order

    list_display = ["account", "date_created", ]
    empty_value_display = "-blank-"
    list_display_links = []
    list_filter = ()
    fieldsets = ()
    ordering = ('date_created', )
    search_fields = ("username", "email")
    filter_horizontal = ()
