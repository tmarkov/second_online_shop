from django.apps import AppConfig


class ShopManagerConfig(AppConfig):
    name = 'shop_manager'
