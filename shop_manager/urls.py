from django.urls import path
from django.contrib.auth.decorators import login_required

from shop_manager import views as shop_manager_views


urlpatterns = [
    path('',
         shop_manager_views.OrderView.as_view(),
         name="order"),
]
