import json

from django.views.generic import View
from django.http import HttpResponse
from shop_manager import models
from django.contrib.auth.mixins import LoginRequiredMixin

from accounts import models as account_models
from shop import models as shop_models


class OrderView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):

        username = request.user
        order_data = json.loads(request.read().decode("utf-8"))

        # Save the order
        new_order = models.Order()
        new_order.account = account_models.Account.objects.get(username=username)
        new_order.save()
        for prod in order_data['products']:
            new_order.products.add(shop_models.Product.objects.get(
                id=prod['pId'])
            )

        return HttpResponse('')
