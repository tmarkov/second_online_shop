from django.urls import path

from shop import views as shop_views


urlpatterns = [
    path('', shop_views.CategoriesView.as_view()),
    path('<int:cat_id>', shop_views.ProductsView.as_view(),
         name="category_products"
         )
]
