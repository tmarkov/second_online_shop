from django.db import models


class Measure(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Category(models.Model):

    class Meta:
        verbose_name_plural = "Categories"

    name = models.CharField(max_length=100)
    parent_category = models.ForeignKey('Category',
                                        null=True,
                                        blank=True,
                                        on_delete=models.PROTECT,
                                        related_name='child_set')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=500, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    photo = models.ImageField(blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    measure_type = models.ForeignKey(Measure, on_delete=models.PROTECT)

    def __str__(self):
        return self.name
