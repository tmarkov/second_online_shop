from django.contrib import admin

from shop import models


@admin.register(models.Category)
class UserAdmin(admin.ModelAdmin):

    list_display = ("name", "parent_category",)
    list_filter = ("parent_category",)

    fieldsets = (
        ("Category Settings", {'fields': ("name", "parent_category",)}),
    )
    ordering = ("name", "parent_category")
    search_fields = ("name", "parent_category")
    filter_horizontal = ()
    empty_value_display = "-blank-"


@admin.register(models.Product)
class UserAdmin(admin.ModelAdmin):

    list_display = ("name", "category", "price", "measure_type",)
    list_filter = ("category",)

    fieldsets = (
        ("Product Information", {'fields': ("name", "description", "price",
                                            "photo", "category",
                                            "measure_type")}),
    )
    ordering = ("name", "category")
    search_fields = ("name", "category")
    filter_horizontal = ()
    empty_value_display = "-blank-"


@admin.register(models.Measure)
class UserAdmin(admin.ModelAdmin):

    list_display = ("name",)
    empty_value_display = "-blank-"
