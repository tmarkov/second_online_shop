from django.views.generic import ListView, TemplateView
from shop import models as shop_models


class CategoriesView(TemplateView):
    template_name = "categories.html"

    def __init__(self):
        super(CategoriesView, self).__init__()
        self.already_in = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_list'] = \
            self._generate_category_tree(shop_models.Category.objects.all())
        return context

    def _generate_category_tree(self, cats):
        if cats:
            if cats[0] not in self.already_in:
                self.already_in.append(cats[0])
                yield cats[0]
                if len(cats[0].child_set.all()) > 0:
                    yield from self._generate_category_tree(
                        cats[0].child_set.all()
                    )
            yield from self._generate_category_tree(cats[1:])
        else:
            # The flag below needs to be added so that the correct number
            # of </ul> closing tags are added in the correct place.
            yield "return"


class ProductsView(ListView):
    model = shop_models.Product

    def get_queryset(self):
        return shop_models.Product.objects.filter(
            category=self.kwargs['cat_id']
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = shop_models.Category.objects.get(
            pk=self.kwargs['cat_id']
        )
        return context
