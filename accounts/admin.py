from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from accounts import models
from accounts.forms import UserCreationForm


@admin.register(models.Account)
class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm

    list_display = ('username', 'email', 'is_admin')
    list_filter = ('is_admin',)

    fieldsets = (
        ("Account Information", {'fields': ("username", "email", "password",
                                            "first_name", "last_name",
                                            "country", "city", "address",)}),
        ("Permissions", {'fields': ("is_admin", "is_staff")})
    )
    ordering = ('username', 'email')
    search_fields = ("username", "email")
    filter_horizontal = ()
    empty_value_display = "-blank-"


admin.site.unregister(Group)
