from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

USERNAME_REGEX = '^[a-z+-]*$'


class MyUserManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(username=username,
                          email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password=None):
        user = self.create_user(username, email, password)
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class Account(AbstractBaseUser):
    username = models.CharField(max_length=50,
                                unique=True,
                                validators=[
                                    RegexValidator(regex=USERNAME_REGEX,
                                                   message="a-z only.",
                                                   code='invalid username'),
                                ])
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=300,
                              unique=True,
                              verbose_name='email address')
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=300)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    def get_short_name(self):
        # The user is identified by their username
        return self.username

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app {{}}?"""
        # Simplest possible answer: Yes, always
        return True
